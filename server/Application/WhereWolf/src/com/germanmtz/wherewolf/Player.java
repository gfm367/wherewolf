package com.germanmtz.wherewolf;

public class Player {
	private int PlayerID;
	private String name;
	private String profilePic;
	private int numVotes;

	// getters and constructors go here
	public Player(int playerId, String name, String profilePicUrl, int numVotes)
	{
		this.PlayerID = playerId;
		this.name = name;
		this.profilePic = profilePicUrl;
		this.numVotes = numVotes;
	}

	public int getPlayerID() {
		return PlayerID;
	}

	public void setPlayerID(int playerID) {
		PlayerID = playerID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProfilePic() {
		return profilePic;
	}

	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}

	public int getNumVotes() {
		return numVotes;
	}

	public void setNumVotes(int numVotes) {
		this.numVotes = numVotes;
	}


}
