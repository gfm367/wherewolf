package edu.utexas.waters.wherewolf;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.content.Intent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

public class InGameActivity extends Activity {
	
	private List<Player> players = new ArrayList<Player>();
	private static final String TAG = "loginactivity";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		System.out.println("Well got here");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_in_game);
		Log.i(TAG, "starting off");
		populatePlayerList();
		populateListView();
//		registerClickCallback();
	}
	private void populatePlayerList() {
		Log.i(TAG, "beginning population");
		players.add(new Player(1, "John", ""+ R.drawable.villager3, 1));
		players.add(new Player(2, "Phil", ""+R.drawable.villager3, 3));
		players.add(new Player(3, "Nathan", ""+R.drawable.villager3, 4));
		Log.i(TAG, "finished populating");
	}
	
	private void populateListView() {
		Log.i(TAG, "populating view1");
		ArrayAdapter<Player> adapter = new MyListAdapter();
		Log.i(TAG, "populating view2");
		ListView list = (ListView) findViewById(R.id.playersInGame);
		Log.i(TAG, "populating view3");
		list.setAdapter(adapter);
		Log.i(TAG, "populating view4");
		Log.i(TAG, "made it past populating view");
		
	}
	
	
//	final TextView t1=new TextView(this); 
//	t1.setText("Hello Android");        
//    final SeekBar sk=(SeekBar) findViewById(R.id.seekBar1);     
//    sk.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {       
//
//	    @Override       
//	    public void onStopTrasckingTouch(SeekBar seekBar) {      
//	        // TODO Auto-generated method stub      
//	    }       
//
//	    @Override       
//	    public void onStartTrackingTouch(SeekBar seekBar) {     
//	        // TODO Auto-generated method stub      
//	    }       
//
//	    @Override       
//	    public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {     
//	        // TODO Auto-generated method stub      
//
//	        t1.setTextSize(progress);
//	        Toast.makeText(getApplicationContext(), String.valueOf(progress),Toast.LENGTH_LONG).show();
//
//	    }       
//	});
//	private void registerClickCallback() {
//		ListView list = (ListView) findViewById(R.id.listOfPlayers);
//		list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//			@Override
//			public void onItemClick(AdapterView<?> parent, View viewClicked,
//					int position, long id) {
//				
//				Car clickedCar = players.get(position);
//				String message = "You clicked position " + position
//								+ " Which is car make " + clickedCar.getMake();
//				Toast.makeText(ShowPlayersActivity.this, message, Toast.LENGTH_LONG).show();
//			}
//		});
//	}
	public void gotoGameSelection(View x){
    	Intent intent = new Intent(this, GameSelectionActivity.class);
    	startActivity(intent);
    }
	private class MyListAdapter extends ArrayAdapter<Player> {
		public MyListAdapter() {
			super(InGameActivity.this, R.layout.single_character, players);
		}

		@Override
		
		public View getView(int position, View convertView, ViewGroup parent) {
			// Make sure we have a view to work with (may have been given null)
			Log.i(TAG, "get into view");
			View itemView = convertView;
			if (itemView == null) {
				itemView = getLayoutInflater().inflate(R.layout.single_character, parent, false);
			}
			
			// Find the car to work with.
			Player currentPlayer = players.get(position);
			Log.i(TAG, "down to prof pic");
			// Get Prof Pic
			
			ImageView img= (ImageView) itemView.findViewById(R.id.profPic);
			Log.i(TAG, "makin moves on prof pic");
			img.setImageResource(R.drawable.villager3);
			Log.i(TAG, "down to prof pic2");
//			ImageView imageView = (ImageView)itemView.findViewById(R.id.profPic);
//			imageView.setImageResource(currentPlayer.getProfilePicUrl());
			Log.i(TAG, "down to name");
			// Name
			TextView makeText = (TextView) itemView.findViewById(R.id.charName);
			makeText.setText(currentPlayer.getName());

			// Vote
			TextView yearText = (TextView) itemView.findViewById(R.id.charVotes);
			yearText.setText("" + currentPlayer.getNumVotes());

			return itemView;
		}				
	}
}