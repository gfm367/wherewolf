package com.germanmtz.wherewolf;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class GameSelectionActivity extends Activity {
	private static final String TAG = "gameselectactivity";
	private int numberOfPresses;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game_selection);	
		
		
		ArrayList<Player> arrayOfPlayers = new ArrayList<Player>();
		
		PlayerAdapter adapter = new PlayerAdapter(this, arrayOfPlayers);
		
		arrayOfPlayers.add(new Player(1, "NightChaser", "malevillager3", 66));
		arrayOfPlayers.add(new Player(2, "CruelDream", "malevillager3", 11));
		arrayOfPlayers.add(new Player(3, "FuzzyPuppy", "malevillager3", 8));
		arrayOfPlayers.add(new Player(4, "ElLobo", "malevillager3", 82));
		arrayOfPlayers.add(new Player(5, "TxLongHorns", "malevillager3", 300));
		
		// Create the adapter to convert the array to views
		//PlayerAdapter adapter = new PlayerAdapter(this, arrayOfPlayers);
		// Attach the adapter to a ListView

		ListView playerListView = (ListView) findViewById(R.id.players_list);
		playerListView.setAdapter(adapter);
		
		
		
		
	}

	@Override
	protected void onStart() {
		numberOfPresses = 0;
		super.onStart();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
	public void gotoCreateGame(View v) {
		Intent intent = new Intent(this, CreateGameActivity.class);
		startActivity(intent);
	}
	
	public void gotoLobby(View v) {
		Intent intent = new Intent(this, GameLobbyActivity.class);
		startActivity(intent);
	}	
	
	
}