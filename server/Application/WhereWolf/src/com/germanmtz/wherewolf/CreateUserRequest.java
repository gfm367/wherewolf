package com.germanmtz.wherewolf;


import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class CreateUserRequest extends BasicRequest {

  public CreateUserRequest (String username, String password)
  {
      super(username, password);
  }
  
  
  /**
  * Put the URL to your API endpoint here
  */
  
  @Override
  public String getURL() {
      return "/v1/register";
  }

  @Override
  public List<NameValuePair> getParameters() {
      return null;
  }

  @Override
  public RequestType getRequestType() {
      return RequestType.POST;
  }

  @Override
  public CreateUserResponse execute(WherewolfNetworking net) {
  
      try {
          JSONObject response = net.sendRequest(this);
          
          if (response.getString("status").equals("success"))
          {
              // int playerID = response.getInt("playerid");
              return new CreateUserResponse("success", "registration in successfully");
          } else {
              
              String errorMessage = response.getString("error");
              return new CreateUserResponse("failure", errorMessage);
          }
      } catch (JSONException e) {
          return new CreateUserResponse("failure", "registration in not working");
      } catch (WherewolfNetworkException ex)
      {
          return new CreateUserResponse("failure", "could not communicate with the server");
      }
      
      
      
  }

}