package com.germanmtz.wherewolf;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class CreateGameActivity extends Activity {
	private static final String TAG = "gameselectactivity";
	private int numberOfPresses;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.create_game);	
	}

	@Override
	protected void onStart() {
		numberOfPresses = 0;
		super.onStart();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
	public void gotoGameSelect(View v1) {
		Intent intent1 = new Intent(this, GameSelectionActivity.class);
		startActivity(intent1);	
	}
}