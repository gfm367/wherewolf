package com.germanmtz.wherewolf;

public class Game {
	private int GameId;
	private String game_name;
	private String admin_name;
	private int numPlayers;
	
	public Game(int GameId, String game_name, String admin_name, int numPlayers){
		
		{
			this.GameId = GameId;
			this.game_name = game_name;
			this.admin_name = admin_name;
			this.numPlayers = numPlayers;
		}
	}

	public int getGameId() {
		return GameId;
	}

	public void setGameId(int gameId) {
		GameId = gameId;
	}

	public String getGame_name() {
		return game_name;
	}

	public void setGame_name(String game_name) {
		this.game_name = game_name;
	}

	public String getAdmin_name() {
		return admin_name;
	}

	public void setAdmin_name(String admin_name) {
		this.admin_name = admin_name;
	}

	public int getNumPlayers() {
		return numPlayers;
	}

	public void setNumPlayers(int numPlayers) {
		this.numPlayers = numPlayers;
	}

}
