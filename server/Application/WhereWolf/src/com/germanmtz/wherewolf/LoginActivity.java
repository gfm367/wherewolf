package edu.utexas.waters.wherewolf;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class LoginActivity extends Activity {
	
	private static final String TAG = "loginactivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        Log.i(TAG, "created the login activity");
    }
    
    public void gotoRegister(View v){
    	Intent intent = new Intent(this, RegisterActivity.class);
    	startActivity(intent);
    }
    
    
    public void gotoGameSelection(View x){
    	new SigninTask().execute();
//    	Intent intent = new Intent(this, GameSelectionActivity.class);
//    	startActivity(intent);
    }
    

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
    public class SigninTask extends AsyncTask<Void, Integer, SigninResponse> {
        @Override
        protected SigninResponse doInBackground(Void... request) {

            final EditText nameTV = (EditText) findViewById(R.id.lastname);
            final EditText passTV = (EditText) findViewById(R.id.password_input);
            
            String username = nameTV.getText().toString();
            String password = passTV.getText().toString();
            System.out.println("username is " + username + " and password is " + password);
            SigninRequest signinRequest = new SigninRequest(username, password);

            return signinRequest.execute(new WherewolfNetworking());
        }

        protected void onPostExecute(SigninResponse result) {

            Log.v(TAG, "Signed in user has player id " + result.getPlayerID());
            System.out.println("result is " + result.getStatus());
            final TextView errorText = (TextView) findViewById(R.id.error_text2);
            
            if (result.getStatus().equals("success")) {
                            
                final EditText nameTV = (EditText) findViewById(R.id.lastname);
                final EditText passTV = (EditText) findViewById(R.id.password_input);
                
                WherewolfPreferences pref = new WherewolfPreferences(LoginActivity.this);
                pref.setCreds(nameTV.getText().toString(), passTV.getText().toString());

                errorText.setText("");
                Log.v(TAG, "Signing in");
                Intent intent = new Intent(LoginActivity.this, GameSelectionActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right,
                        R.anim.slide_out_left);
            } else {
                // do something with bad password
                
                errorText.setText(result.getErrorMessage());
            }

        }

        

    }
}
