
package com.germanmtz.wherewolf;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class MainScreenActivity extends Activity {
	private static final String TAG = "gameselectactivity";
	private int numberOfPresses;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.screen_main);
		
		
		ArrayList<Player> arrayOfPlayers = new ArrayList<Player>();
		
		PlayerAdapter adapter = new PlayerAdapter(this, arrayOfPlayers);
		
		arrayOfPlayers.add(new Player(1, "Erik", "malevillager3", 5));
		arrayOfPlayers.add(new Player(2, "Hayden", "malevillager3", 3));
		arrayOfPlayers.add(new Player(3, "Jonathan", "malevillager3", 1));
		arrayOfPlayers.add(new Player(4, "Eddy", "malevillager3", 0));
		
		// Create the adapter to convert the array to views
		//PlayerAdapter adapter = new PlayerAdapter(this, arrayOfPlayers);
		// Attach the adapter to a ListView

		ListView playerListView = (ListView) findViewById(R.id.players_list);
		playerListView.setAdapter(adapter);
		
		Timer timer = new Timer();
		//timer.schedule(new UpdateTimeTask(), 1000, 5*60000);
		
		
	}

	@Override
	protected void onStart() {
		numberOfPresses = 0;
		super.onStart();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
	public void gotoMain(View v1) {
		Intent intent1 = new Intent(this, MainScreenActivity.class);
		startActivity(intent1);	
	}
	
	
	
}