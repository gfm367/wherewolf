package com.germanmtz.wherewolf;

public class ChangedLocationResponse extends BasicResponse {
	 

private final long currentTime;

 

// list of nearby players

// any important game messages (killed, voted off)

 

public ChangedLocationResponse(String status, String errorMessage) {

super(status, errorMessage);

currentTime = 0;

}

 

// Overloaded constructor

public ChangedLocationResponse(String status, String errorMessage, 

long currentTime) {

 

super(status, errorMessage);

this.currentTime = currentTime;

}

 

public long getCurrentTime() {

return currentTime;

}
}