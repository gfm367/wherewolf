package com.germanmtz.wherewolf;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


public class GameLobbyActivity extends Activity {
	private static final String TAG = "gameselectactivity";
	private int numberOfPresses;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lobby_game);	
		
		
		ArrayList<Player> arrayOfPlayers = new ArrayList<Player>();
		
		PlayerAdapter adapter = new PlayerAdapter(this, arrayOfPlayers);
		
		arrayOfPlayers.add(new Player(1, "Erik", "malevillager3", 5));
		arrayOfPlayers.add(new Player(2, "Hayden", "malevillager3", 3));
		arrayOfPlayers.add(new Player(3, "Jonathan", "malevillager3", 1));
		arrayOfPlayers.add(new Player(4, "Eddy", "malevillager3", 0));
		
		// Create the adapter to convert the array to views
		//PlayerAdapter adapter = new PlayerAdapter(this, arrayOfPlayers);
		// Attach the adapter to a ListView

		ListView playerListView = (ListView) findViewById(R.id.players_list);
		playerListView.setAdapter(adapter);
		
	}

	@Override
	protected void onStart() {
		numberOfPresses = 0;
		super.onStart();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
	public void gotoMainScreen(View v1) {
		Intent intent1 = new Intent(this, MainScreenActivity.class);
		startActivity(intent1);	
	}
	
}