package com.germanmtz.wherewolf;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class GameAdapter extends ArrayAdapter<Player>{
    public GameAdapter(Context context, ArrayList<Player> players) {
        super(context, 0, players);
     }

     @Override
     public View getView(int position, View convertView, ViewGroup parent) {
    	 
        // Get the data item for this position
    	 
        Player player = getItem(position);    
        
        // Check if an existing view is being reused, otherwise inflate the view
        
        if (convertView == null) {
           convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_player, parent, false);
        }
        
        // Lookup view for data population
        
        ImageView profileImg = (ImageView) convertView.findViewById(R.id.playerimg);
        TextView nameTV = (TextView) convertView.findViewById(R.id.player_name);
        TextView votesTV = (TextView) convertView.findViewById(R.id.player_votes);
        
        
        // Populate the data into the template view using the data object
        

        {
        	profileImg.setImageResource(R.drawable.villager3);
        }
        
        nameTV.setText(player.getName());
        votesTV.setText(""+player.getNumVotes());
        
        // Return the completed view to render on screen
        return convertView;
    }
}

