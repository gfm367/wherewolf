package com.germanmtz.wherewolf;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class RegisterActivity extends Activity {

	private static final String TAG = "registeractivity";
	public void stopRegistering()
	{
		Log.v(TAG, "closing the register screen");
		this.finish();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register_layout);
	
		final Button button = (Button) findViewById(R.id.register_user_button);

		View.OnClickListener jim = new View.OnClickListener() {
			public void onClick(View v) {
				Log.v(TAG, "TESTTTT");
				stopRegistering();
			}
		};
		
		button.setOnClickListener(jim);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	
	
	
	

	
	
	
	
	private class RegisterTask extends AsyncTask<Void, Integer, CreateUserResponse> {

	      @Override
	      protected CreateUserResponse doInBackground(Void... request) {

	          final EditText nameTV = (EditText) findViewById(R.id.username_input);
	          final EditText passTV = (EditText) findViewById(R.id.password_input);
	          
	          String username = nameTV.getText().toString();
	          String password = passTV.getText().toString();
	          
	          CreateUserRequest CreateUserRequest = new CreateUserRequest(username, password);
	          
	          return CreateUserRequest.execute(new WherewolfNetworking());
	          
	      
	      }

	      protected void onPostExecute(CreateUserResponse result) {

	          //Log.v(TAG, "Signed in user has player id " + result.getPlayerID());
	          
	          final TextView errorText = (TextView) findViewById(R.id.error_text);
	          
	          if (result.getStatus().equals("success")) {
	                          
	              final EditText nameTV = (EditText) findViewById(R.id.username_input);
	              final EditText passTV = (EditText) findViewById(R.id.password_input);
	              
	              WherewolfPreferences pref = new WherewolfPreferences(RegisterActivity.this);
	              pref.setCreds(nameTV.getText().toString(), passTV.getText().toString());

	              errorText.setText("");
	              Log.v(TAG, "Signing in");
	              Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
	              startActivity(intent);
	              //overridePendingTransition(R.anim.slide_in_right,
	                //      R.anim.slide_out_left);
	          } else {
	              // do something with bad password
	              
	              errorText.setText(result.getErrorMessage());
	          }

	      }
	
	
}
	
}
