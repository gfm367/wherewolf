package com.germanmtz.wherewolf;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {
	private static final String TAG = "loginactivity";
	private int numberOfPresses; 	

	private void startRegisterActivity()   {
		Intent intent = new Intent(this, RegisterActivity.class);
		startActivity(intent);
	}
	
	
	private void startGameSelectActivity() {
		new SigninTask().execute();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		final Button button = (Button) findViewById(R.id.registerButton);

		View.OnClickListener jim = new View.OnClickListener() {
			public void onClick(View v) {
				startRegisterActivity();
			}
		};

		button.setOnClickListener(jim);
		/*
		 * button.setOnClickListener(new View.OnClickListener() { public void
		 * onClick(View v) { // Perform action on click } });
		 */
	
		final Button Loginbutton = (Button) findViewById(R.id.loginButton);

		View.OnClickListener tom = new View.OnClickListener() {
			public void onClick(View v) {
				startGameSelectActivity();
			}
		};

		Loginbutton.setOnClickListener(tom);
		
		/*
		 * button.setOnClickListener(new View.OnClickListener() { public void
		 * onClick(View v) { // Perform action on click } });
		 */
	}

	@Override
	protected void onStart() {
		numberOfPresses = 0;
		super.onStart();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
	public void gotoGameSelect(View v1) {
		Intent intent1 = new Intent(this, GameSelectionActivity.class);
		startActivity(intent1);	
	}
	
	
	private class SigninTask extends AsyncTask<Void, Integer, SigninResponse> {

	      @Override
	      protected SigninResponse doInBackground(Void... request) {

	          final EditText nameTV = (EditText) findViewById(R.id.username_input);
	          final EditText passTV = (EditText) findViewById(R.id.password_input);
	          
	          String username = nameTV.getText().toString();
	          String password = passTV.getText().toString();
	          
	          SigninRequest signinRequest = new SigninRequest(username, password);
	          
	          return signinRequest.execute(new WherewolfNetworking());
	          
	      }

	      protected void onPostExecute(SigninResponse result) {

	          Log.v(TAG, "Signed in user has player id " + result.getPlayerID());
	          
	          final TextView errorText = (TextView) findViewById(R.id.error_text);
	          
	          if (result.getStatus().equals("success")) {
	                          
	              final EditText nameTV = (EditText) findViewById(R.id.username_input);
	              final EditText passTV = (EditText) findViewById(R.id.password_input);
	              
	              WherewolfPreferences pref = new WherewolfPreferences(MainActivity.this);
	              pref.setCreds(nameTV.getText().toString(), passTV.getText().toString());

	              errorText.setText("");
	              Log.v(TAG, "Signing in");
	              Intent intent = new Intent(MainActivity.this, GameSelectionActivity.class);
	              startActivity(intent);
	              //overridePendingTransition(R.anim.slide_in_right,
	                //      R.anim.slide_out_left);
	          } else {
	              // do something with bad password
	              
	              errorText.setText(result.getErrorMessage());
	          }

	      }
	      
	  }
	
}
