/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.germanmtz.wherewolf;

public final class R {
    public static final class attr {
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Example customization of dimensions originally defined in res/values/dimens.xml
         (such as screen margins) for screens with more than 820dp of available width. This
         would include 7" and 10" devices in landscape (~960dp and ~1280dp respectively).
    
         */
        public static final int activity_horizontal_margin=0x7f040000;
        public static final int activity_vertical_margin=0x7f040001;
    }
    public static final class drawable {
        public static final int cloud2=0x7f020000;
        public static final int cloud3=0x7f020001;
        public static final int day=0x7f020002;
        public static final int dusk1=0x7f020003;
        public static final int dusk2=0x7f020004;
        public static final int ic_launcher=0x7f020005;
        public static final int moon3=0x7f020006;
        public static final int mybutton=0x7f020007;
        public static final int night=0x7f020008;
        public static final int orangebutton=0x7f020009;
        public static final int purplebg=0x7f02000a;
        public static final int sun=0x7f02000b;
        public static final int villager3=0x7f02000c;
        public static final int werewolflogo=0x7f02000d;
        public static final int wherewolfbg=0x7f02000e;
        public static final int wherewolfdesign3=0x7f02000f;
        public static final int wwlogo=0x7f020010;
    }
    public static final class id {
        public static final int EditText01=0x7f080012;
        public static final int LinearLayout1=0x7f080000;
        public static final int action_settings=0x7f080016;
        public static final int button1=0x7f08000a;
        public static final int button2=0x7f08000b;
        public static final int editText1=0x7f080009;
        public static final int editText2=0x7f080008;
        public static final int error_text=0x7f080007;
        public static final int imageView2=0x7f080001;
        public static final int loginButton=0x7f080004;
        public static final int needtoregister=0x7f080005;
        public static final int passwordText=0x7f080013;
        public static final int password_input=0x7f080003;
        public static final int player_name=0x7f08000e;
        public static final int player_votes=0x7f08000f;
        public static final int playerimg=0x7f08000d;
        public static final int players_list=0x7f08000c;
        public static final int registerButton=0x7f080006;
        public static final int register_layout=0x7f080010;
        public static final int register_user_button=0x7f080014;
        public static final int seekBar1=0x7f080015;
        public static final int usernameText=0x7f080011;
        public static final int username_input=0x7f080002;
    }
    public static final class layout {
        public static final int activity_login=0x7f030000;
        public static final int create_game=0x7f030001;
        public static final int game_selection=0x7f030002;
        public static final int item_player=0x7f030003;
        public static final int lobby_game=0x7f030004;
        public static final int register_layout=0x7f030005;
        public static final int screen_main=0x7f030006;
        public static final int xxactivity_main=0x7f030007;
    }
    public static final class menu {
        public static final int login=0x7f070000;
        public static final int main=0x7f070001;
    }
    public static final class string {
        public static final int Log_In=0x7f050009;
        public static final int Password=0x7f050007;
        public static final int Register=0x7f050003;
        public static final int Username=0x7f050005;
        public static final int action_settings=0x7f050002;
        public static final int app_name=0x7f050000;
        public static final int create=0x7f05000c;
        public static final int create_game=0x7f050010;
        public static final int description=0x7f05000e;
        public static final int error=0x7f050014;
        public static final int g_name=0x7f05000f;
        public static final int hello_world=0x7f050001;
        public static final int join=0x7f05000d;
        public static final int login=0x7f05000a;
        public static final int name1=0x7f050012;
        public static final int name2=0x7f050013;
        public static final int needtoregister=0x7f05000b;
        public static final int password=0x7f050008;
        public static final int register=0x7f050004;
        public static final int start=0x7f050011;
        public static final int username=0x7f050006;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f060000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f060001;
    }
}
